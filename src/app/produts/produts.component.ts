import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


import { ProductService } from './product.service';
import { PRODUCTS } from '../mock-product';


@Component({
  selector: 'app-produts',
  templateUrl: './produts.component.html',
  styleUrls: ['./produts.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProdutsComponent implements OnInit {

  // data: any;
  products = PRODUCTS;
  product: any;

  constructor(private productService: ProductService, private http: HttpClient, private router: Router) { }
  ngOnInit() {

    // this.productService.sendGetRequest().subscribe((data: any)=>{
    //   console.log(data);
    //   this.products = data;
    // })  
  }

  onSelect(): void {
    this.router.navigate(['/product-show']);
  }
}
