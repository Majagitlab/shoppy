import { Component, OnInit } from '@angular/core';


import { PRODUCTS } from '../mock-product';

@Component({
  selector: 'app-product-show',
  templateUrl: './product-show.component.html',
  styleUrls: ['./product-show.component.scss']
})
export class ProductShowComponent implements OnInit {
  products = PRODUCTS;
  
  constructor() { }

  ngOnInit(): void {
  }

}
