import { Product } from './product';

export const PRODUCTS: Product[] = [
  { id: 1, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_1.png' },
  { id: 2, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_2.png' },
  { id: 3, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_2.png' },
  { id: 4, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_1.png' },
  { id: 5, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_3.png' },
  { id: 6, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_2.png' },
  { id: 7, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_1.png' },
  { id: 8, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_3.png' },
  { id: 9, name: 'Reebok Track Jacket', price: '100 $', img: './assets/img/Products_2.png' }
];